// Iniciamos la segunda parte con 
// Object Destructuring
console.log("Object Destructuring");

// Es ni más ni menos la manera de acceder a los valores 
// de un objeto.

const computadorasOchentosas = {
    sinclair: {
        inicial: "zx-81",
        evolucion: "spectrum",
        final: "2068"
    },
    otras: ["Commodore 64", "Atari Amiga", "Texas TI-99 4A"]
}

// Acá vemos cómo se ha definido el objeto
console.log (computadorasOchentosas);

// Y accedemos

let primeraComputadora = computadorasOchentosas.sinclair.inicial;
let deseada = computadorasOchentosas.otras[0];

console.log(`Mi primera computadora fue una ${primeraComputadora} y todos deseábamos
la ${deseada}`);

// Ahora con Object Destructuring es más fácil
// Ya toma solo el valor si uso el mismo nombre del atributo

let {inicial} = computadorasOchentosas.sinclair;
console.log(`Esto es usando Object Destructuring y traigo ${inicial}`);

// Object literal enhacement
// Unir variables en un objeto
// Algo así como lo contrario a lo de recién

console.log("Object Literal Enhacement");

let director = "Quentin Tarantino";
let genero = "Acción";
let peliculas = ["Kill Bill", "Death Proof", "Jackie Brown"];


// Hacerlo a la manera antigua

const quentin = {
    nombre: director,
    genero: genero,
    pelis: peliculas
};

console.log(quentin);

// Ahora lo hacemos con OLE

const quentin_ole = {director, genero, peliculas};
console.log(quentin_ole);

// Cuando creo un objeto, también le puedo agregar funciones propias 
// en la descripción. O sea que aparte de los atributos, le genero 
// métodos a ese objeto. Usando "this" no tengo que pasar parámetros
// a esta función, ya los toma de la descripción del mismo. Tampoco es necesario 
// usar la palabra reservada "function".

console.log("Creando métodos de un objeto");

const obiWan = {
    nombre: "Obi Wan Kenobi",
    titulo: "Master Jedi",
    armado: true,
    arma: "Sable laser",
    descripcion(){
        console.log(`El personaje ${this.nombre} es ${this.titulo} y está armado con ${this.arma}`);
    }
} 

// También podría haber usado 
// la expresión 
// descripcion: function(){
//   console.log(`El personaje ${this.nombre} es ${this.titulo} y está armado con ${this.arma}`);


obiWan.descripcion();


// Generar HTML con javaScript
// Acordarse de la regla "contenedor/contenido" para la descripción
// manera tradicional

let deportes_olimpicos = ["Remo", "Atletismo", "Lucha Libre", "Tenis de mesa"];

// Vemos el array
console.log(deportes_olimpicos);

// Defino un contenedor, lo apunto según el id que le puse en el documento html

const contenedor = document.querySelector("#app");

// Defino el contenido, primero como una cadena vacía

let html = '';

// Y luego creo la cadena, iterando sobre el array
// Hay varias maneras, (while - for)
// Vamos a usar por ejemplo el forEach
// Itera sobre los elementos del array y la variable "deporte" va tomando el valor
// de esos elementos.

deportes_olimpicos.forEach(deporte => {
    html += `<li>${deporte}</li>`;
});

// Y luego lo "renderizamos" con innerHTML

contenedor.innerHTML = html;


// Ahora veamos el map
// que es mucho más simple 

 deportes_olimpicos.map(deporte => {
    console.log("El deporte es " + deporte);
 });



// Hay veces que necesito traer el valor de las claves, 
// no el contenido de las mismas. Para eso uso Object Keys
// veamos sobre el ejemplo de Obi Wan

console.log(Object.keys(obiWan));


// Algunas operaciones con Arrays
console.log("Operaciones con Arrays");

// Definimos dos arreglos

heroes = ["Leia", "Han", "Luke"];
villanos = ["Jabba", "Darth Maul", "Lord Vader"];

// Forma tradicional de concatenarlos

let combinado = heroes.concat(villanos);
console.log(combinado); 

// Forma nueva

let combinado_nuevo = [...heroes, ...villanos];
console.log(combinado_nuevo); 

// Y también se usa esto de los tres puntos
// para duplicar un array y generar otro

let dupli_heroes = [...heroes];
console.log("Duplicados");
console.log(dupli_heroes);

// Ahora vamos a jugar un poco con el arreglo. 
// el método reverse() da vuelta el array y devuelve 
// el último (antes de la inversión)

let [editado] = heroes.reverse();

console.log(editado);
console.log(heroes);

// Para que no pase esto 

let [editado_dos] = [...heroes].reverse();

console.log(editado_dos);
console.log(heroes);


// También se usa para operaciones
// Por cierto, esto de los ... se llama Spread Operator,
// no lo había puesto antes

let suma =  (primero, segundo, tercero) => {
    console.log(primero + segundo + tercero);
};

numeros = [4,5,6];

suma(...numeros);

// Más métodos con arrays
// Vamos con este ejemplo

let personalidades = [
    {nombre: "Mozart", area: "Música", nacio: 1756, pais: "Salzburgo"},
    {nombre: "Einstein", area: "Física", nacio: 1879, pais: "Alemania"},
    {nombre: "Tesla", area: "Electricidad", nacio: 1856, pais: "Imperio Austro Húngaro"},
    {nombre: "Ada Lovelace", area: "Matemática", nacio: 1815, pais: "Inglaterra"},
    {nombre: "Lola Mora", area: "Escultura", nacio: 1866, pais: "Argentina"},
];

console.log(personalidades);

// Filtrar por año
// Nacieron luego de 1850
// con filter itera sobre todos los elementos

personalidades.filter(personalidad => {
    console.log(personalidad);
});

// Ahora vamos a filtrar los que el año de nacimiento
// sea mayor a 1850

const mayores = personalidades.filter(personalidad => {
    return personalidad.nacio > 1850;
});

console.log(mayores);

// Queremos traer los datos de Tesla

const tesla = personalidades.find(personalidad => {
    return personalidad.nombre === "Tesla";
});

console.log("Nikola tesla se dedicó al área de " + tesla.area);


// Y ahora un poco de magia
// vamos a sumar los años con "reduce"

let total_anios = personalidades.reduce((anios_total, personalidad) => {
    return anios_total + personalidad.nacio;
}, 0);

console.log ("Si sumamos los años de nacimiento de todxs nos da " + total_anios );
console.log("El año promedio de nacimiento es " + (total_anios/personalidades.length));

// Ahora el tema que había quedado, las Promises
// Intro: Cuando hacés un pedido asincrónico a una API tenés dos posibilidades
// 1) que todo salga OK. "resolve"
// 2) que algo salga mal. (El famoso "Puede fallar" de Tu-Sam). "reject"
// 

let darDeBaja = new Promise((resolve, reject) => {
    // Para simular una consulta
    // con timeout y error 
    
    setTimeout(() => {
        let baja = true;

        if (baja) {
            resolve("Se ha dado de baja");
        } else {
            reject("No se ha podido dar de baja");
        }
    }, 2500);
});

console.log (darDeBaja);

// en el caso anterior si vamos a la consola vemos que el estado es "pendiente"
// eso quiere decir que venimos planteando bien pero no estamos listxs todavía
// pasemos al siguiente paso


darDeBaja.then(resultado_consulta => {
    console.log(resultado_consulta);
})


// Ahora vamos a hacer un ejemplo real, ya que había quedado medio rengo
// lo voy a hacer paso por paso para intentar que no haya dudas
// Usaré la API pública de randomuser.me/api que te baja un JSON con usuarios
// random. Muy buena para practicar.

const traerUsers = cantidad => new Promise((resolve, reject) => {

    // Vamos por partes
    // Primero la url de la api

    const url = `https://randomuser.me/api/?results=${cantidad}&nat=us`;

    // Segundo el llamado a AJAX
    const xhr = new XMLHttpRequest();

    // Tercero, abrimos la conexión
    // El "true" es si es síncrono

    xhr.open('GET', url, true);

    // Cuarto, estamos ya trayendo los registros
    // El 200 es el código que todo salió bien
    // El "results" viene del JSON de la API.
    // esta es la parte más compleja.

    xhr.onload = () => {
        if(xhr.status === 200) {
            resolve(JSON.parse(xhr.responseText).results);
        }else{
            reject(Error(xhr.statusText));
        };
    };

    // Quinto opcional - On Error
    xhr.onerror = (error) => reject(error);

    // Quinto o sexto, parte final, enviarlo
    xhr.send();
});

// Y armamos todo para sacarlo por la consola

traerUsers(15).then(
    usuarios => console.log(usuarios),
    error => console.error(
        new Error("Se produjo el error" + error),
    )
    );

// Ahora veamos cómo se puede recorrer el array para 
// que nos muestre cada registro
// Cambio primero la traerUser por imprimirUsers
// y le asigno la salida a una función generarHTML en lugar
// de tirarlos a la consola

traerUsers(10).then(
    registros => generarHTML(registros),
    error => console.error(
        new Error("Se produjo el error" + error),
    )
    );

// Ahora defino la generarlHTML

function generarHTML(registros) {
    let html_generado = "";    
    registros.forEach(registro => {
        console.log(registro);
        html_generado += `
            <li>
            <b>
                Nombre: ${registro.name.first} ${registro.name.last}
            </b>   
                País: ${registro.nat}
                Imagen:
                    <img src="${registro.picture.medium}" />
            </li>
            `;

// Acordarse de la lógica "dónde lo ubico, qué ubico"
    const contenedorAPI = document.querySelector('#app_api');
    contenedorAPI.innerHTML = html_generado; 
            
    })




};
